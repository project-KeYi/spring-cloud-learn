package com.pjq.springcloud.servie.impl;

import com.pjq.springcloud.dao.DeptDao;
import com.pjq.springcloud.pojo.Dept;
import com.pjq.springcloud.servie.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author JiaQing
 * @className DeptServiceImpl
 * @Date 2021/4/10 20:11
 * @Version 1.0
 **/
@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptDao deptDao;

    @Override
    public boolean addDept(Dept dept) {
        return deptDao.addDept(dept);
    }

    @Override
    public Dept queryId(Long id) {
        return deptDao.queryId(id);
    }

    @Override
    public List<Dept> queryAll() {
        return deptDao.queryAll();
    }
}
