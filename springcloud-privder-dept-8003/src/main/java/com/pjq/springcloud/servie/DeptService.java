package com.pjq.springcloud.servie;

import com.pjq.springcloud.pojo.Dept;

import java.util.List;

/**
 * @author JiaQing
 * @className DeptService
 * @Date 2021/4/10 20:11
 * @Version 1.0
 **/
public interface DeptService {

    public boolean addDept(Dept dept);

    public Dept queryId(Long id);

    public List<Dept> queryAll();
}
