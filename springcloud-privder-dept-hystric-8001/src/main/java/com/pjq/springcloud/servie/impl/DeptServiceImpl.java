package com.pjq.springcloud.servie.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.pjq.springcloud.dao.DeptDao;
import com.pjq.springcloud.pojo.Dept;
import com.pjq.springcloud.servie.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author JiaQing
 * @className DeptServiceImpl
 * @Date 2021/4/10 20:11
 * @Version 1.0
 **/
@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptDao deptDao;

    @Override
    public boolean addDept(Dept dept) {
        return deptDao.addDept(dept);
    }

    @Override
    @HystrixCommand(fallbackMethod = "hystricQuery")
    public Dept queryId(Long id) {
        Dept dept = deptDao.queryId(id);
        if (dept == null){
            throw new RuntimeException("id查不到对应的对象值");
        }

        return dept;
    }

    /**
     * 备选的方法
     * @param id
     * @return
     */
    public Dept hystricQuery(@PathVariable("id") Long id){
        Dept dept = new Dept();
        return dept.setDeptno(id)
                .setDname("id>>>"+id+"查询不到对应的值")
                .setDb_source("this database is no data");

    }

    @Override
    public List<Dept> queryAll() {
        return deptDao.queryAll();
    }
}
