package com.pjq.springcloud.controller;

import com.pjq.springcloud.pojo.Dept;
import com.pjq.springcloud.service.DeptClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author JiaQing
 * @className DeptConsumerController
 * @Date 2021/4/10 20:42
 * @Version 1.0
 **/

@RestController
public class DeptConsumerController {

    @Autowired
    private DeptClientService deptClientService;

    @RequestMapping("/consumer/dept/add")
    public boolean add(@RequestBody Dept dept){

        boolean b = deptClientService.addDept(dept);
        return  b;

    }


    @RequestMapping(value = "/consumer/dept/get/{id}",produces = {MediaType.APPLICATION_JSON_VALUE})
    public Dept get(@PathVariable("id") Long id){

        System.err.println("得到的id:"+id);

        Dept dept = deptClientService.queryId(id);

        System.err.println(dept.toString());

        return  dept;
    }

    @RequestMapping("/consumer/dept/getList")
    public List<Dept> getList(){

        System.err.println("获取所有的信息");

      return  deptClientService.queryAll();
    }
}
