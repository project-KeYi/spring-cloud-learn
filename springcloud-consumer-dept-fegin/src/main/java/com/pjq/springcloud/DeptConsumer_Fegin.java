package com.pjq.springcloud;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;



/**
 * @author JiaQing
 * @className DeptConsumer_80
 * @Date 2021/4/10 21:02
 * @Version 1.0
 **/
// ribbon 和 eureka整合以后，客户端可以直接调用，不用关心端口号
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.pjq.springcloud"})
public class DeptConsumer_Fegin {

    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer_Fegin.class,args);
    }


}
