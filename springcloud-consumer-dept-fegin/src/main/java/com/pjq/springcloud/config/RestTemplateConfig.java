package com.pjq.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author JiaQing
 * @className RestTemplateConfig
 * @Date 2021/4/10 20:46
 * @Version 1.0
 **/
@Configuration
public class RestTemplateConfig {

    //iRule

    //RoundRobinRule 轮训
    //RandomRule 随机
    //WeightedResponseTimeRule 权重

    @Bean
    @LoadBalanced  //ribbon 默认轮训
    public RestTemplate getRestTemplate(){

        return  new RestTemplate();
    }
}
