package com.pjq.springcloud.service;

import com.pjq.springcloud.pojo.Dept;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author JiaQing
 * @className hystricDeptClientService
 * @Date 2021/4/14 22:39
 * @Version 1.0
 **/
@Component
public class hystricDeptClientService implements FallbackFactory {

    /**
     * 实现fallbackFactory 重写create方法 实现熔断降级
     * @param throwable
     * @return
     */
    @Override
    public DeptClientService create(Throwable throwable) {

        DeptClientService deptClientService = new DeptClientService() {
            @Override
            public boolean addDept(Dept dept) {
                return false;
            }

            @Override
            public Dept queryId(Long id) {
                return new Dept().setDeptno(id)
                        .setDname("id=>" + id + "没有数据，服务可以关闭，提供了降级的处理")
                        .setDb_source("this database is no data");
            }

            @Override
            public List<Dept> queryAll() {
                return null;
            }
        };

       return deptClientService;
    }
}
