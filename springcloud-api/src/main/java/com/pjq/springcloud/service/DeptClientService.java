package com.pjq.springcloud.service;

import com.pjq.springcloud.pojo.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Component
@FeignClient(value = "SPRINGCLOUD-PRIVDER-DEPT",url = "http://127.0.01:8001",fallbackFactory = hystricDeptClientService.class)
public interface DeptClientService {


    @PostMapping("/dept/add")
    public boolean addDept(@RequestBody Dept dept);

    @GetMapping("/dept/get/{id}")
    public Dept queryId(@PathVariable("id") Long id);

    @GetMapping("/dept/getList")
    public List<Dept> queryAll();
}
