package com.pjq.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author JiaQing
 * @className EurekaServer_7001
 * @Date 2021/4/11 12:38
 * @Version 1.0
 **/
//启动之后，访问 http://localhost:7001/
@SpringBootApplication
@EnableEurekaServer  //Eureka 服务端注册
public class EurekaServer_7003 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServer_7003.class,args);
    }
}
