package com.pjq.springcloud.controller;

import com.pjq.springcloud.pojo.Dept;
import com.pjq.springcloud.servie.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author JiaQing
 * @className DeptController
 * @Date 2021/4/10 20:15
 * @Version 1.0
 **/
//提供Resuful 服务
@RestController
public class DeptController {

    @Autowired
    private DeptService deptService;

    //获取一些配置的信息
    @Autowired
    private DiscoveryClient discoveryClient;


    @PostMapping("/dept/add")
    public boolean addDept(@RequestBody Dept dept){
        System.err.println(dept);
        return  deptService.addDept(dept);

    }

    @GetMapping("/dept/get/{id}")
    public Dept getDept(@PathVariable("id") Long id){
        return  deptService.queryId(id);

    }

    @GetMapping("/dept/getList")
    public List<Dept> queryAll(){
        return  deptService.queryAll();

    }

    //注册进来的微服务，获取一些信息
    @GetMapping("/get/discovery")
    public Object discovery(){
        //获取微服务列表的清单
        List<String> services = discoveryClient.getServices();
        System.out.println("services = " + services);

        //得到其中一个微服务的信息
        List<ServiceInstance> instances = discoveryClient.getInstances("SPRINGCLOUD-PRIVDER-DEPT");
        for (ServiceInstance instance : instances) {
            System.err.println(
                    "获取的微服务信息:"+
                      instance.getHost()+"\t"+
                      instance.getPort()+"\t"+
                      instance.getUri()+"\t"+
                      instance.getServiceId()


            );
        }

        return this.discoveryClient;

    }
}
