package com.pjg.springcloud.controller;

import com.pjq.springcloud.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author JiaQing
 * @className DeptConsumerController
 * @Date 2021/4/10 20:42
 * @Version 1.0
 **/

@RestController
public class DeptConsumerController {

    //理解：消费者，不应该有service层
    // RestTemplate 供我们直接调用
    //(url，实体 )

    public static final String REST_URL_PREFIX = "http://SPRINGCLOUD-PRIVDER-DEPT";

    @Autowired
    private RestTemplate  restTemplate;

    @RequestMapping("/consumer/dept/add/{dname}")
    public boolean add(@PathVariable("dname") String dname){
        Dept dept = new Dept();
        dept.setDname("小红");
        System.err.println(dname);

        return  restTemplate.postForObject(REST_URL_PREFIX+"/dept/add",dept,Boolean.class);
    }


    @RequestMapping("/consumer/dept/get/{id}")
    public Dept get(@PathVariable("id") Long id){

        Dept dept = restTemplate.getForObject(REST_URL_PREFIX + "/dept/get/" + id, Dept.class);
        System.err.println("dept:"+ dept);
        return  dept;
    }

    @RequestMapping("/consumer/dept/getList")
    public List<Dept> getList(){

        return restTemplate.getForObject(REST_URL_PREFIX+"/dept/getList",List.class);
    }
}
