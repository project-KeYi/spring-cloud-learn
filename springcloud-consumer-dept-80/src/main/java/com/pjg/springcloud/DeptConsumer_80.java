package com.pjg.springcloud;

import com.pjg.ribbonConfig.RibbonConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @author JiaQing
 * @className DeptConsumer_80
 * @Date 2021/4/10 21:02
 * @Version 1.0
 **/
// ribbon 和 eureka整合以后，客户端可以直接调用，不用关心端口号
@SpringBootApplication
@EnableEurekaClient
@RibbonClient(name = "SPRINGCLOUD-PRIVDER-DEPT",configuration = RibbonConfig.class)
public class DeptConsumer_80 {

    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer_80.class,args);
    }


}
