package com.pjg.ribbonConfig;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author JiaQing
 * @className ribbonConfig
 * @Date 2021/4/13 22:53
 * @Version 1.0
 **/
@Configuration
public class RibbonConfig {


    @Bean
    public IRule myRule(){

        return  new RandomRule();
    }
}
